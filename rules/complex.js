function (ctx, callback) {
	//test 5
	if (!ctx.user.password || ctx.user.password.length < 8) {
		return callback(new UnauthorizedError('Password does not match the password policy.'));
	}

	// i need something really big for testing...
	// Lorem ipsum dolor sit amet, vim primis conceptam intellegat ad, accusam volutpat ut pri. Prima suscipit insolens duo ex, ea vide eius euripidis ius, per munere vivendo persequeris id. Eum ne sanctus epicurei, eam sale soleat contentiones ne. Te zril aeterno admodum pri, ea nec principes incorrupte.

  callback(null, ctx);
}
