function (ctx, callback) {
	if (!ctx.user.password || ctx.user.password.length < 8) {
		return callback(new UnauthorizedError('Password does not match the password policy.'));
	}

	callback(null, ctx);
}
